import React, { ReactNode, useState } from "react";
import { Modal } from "shared/ui";

interface IModalContext {
  modal: ReactNode;
  openModal: (content: ReactNode) => void;
  closeModal: () => void;
}

export const ModalContext = React.createContext({} as IModalContext);

interface IModalProviderProps {
  children: ReactNode;
}

function ModalProvider({ children }: IModalProviderProps) {
  const [modal, setModal] = useState<ReactNode>(null);

  const openModal = (content: ReactNode) => {
    setModal(content);
  };

  const closeModal = () => {
    setModal(null);
  };

  return (
    <ModalContext.Provider value={{ modal, openModal, closeModal }}>
      {children}
      {modal && <Modal>{modal}</Modal>}
    </ModalContext.Provider>
  );
}

export default ModalProvider;
