import { RouterProvider, createBrowserRouter } from "react-router-dom";
import "./index.scss";

import { Root } from "pages";
import ModalProvider from "./context";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <div>Error Page</div>,
    children: [
      {
        path: "",
        element: <Root.About />,
      },
      {
        path: "quotes",
        element: <Root.Quotes />,
      },
    ],
  },
]);

const App = () => (
  <div className="app">
    <ModalProvider>
      <RouterProvider router={router} />
    </ModalProvider>
  </div>
);

export default App;
