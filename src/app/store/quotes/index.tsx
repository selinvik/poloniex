import { IQuote } from "entities/quote/types";
import { makeAutoObservable } from "mobx";
import { fetchQuotes } from "shared/api/quotes";

class Quotes {
  quotes: IQuote[] = [];
  isLoading: boolean = false;
  isError: boolean = false;

  constructor() {
    makeAutoObservable(this);
  }

  setIsLoading(state: boolean) {
    this.isLoading = state;
  }

  async initialFetchQuotes() {
    this.isLoading = true;
    const response = await fetchQuotes("/public?command=returnTicker");
    this.isLoading = false;
    if (response.data.error) {
      this.quotes = [];
      this.isError = true;
    } else {
      this.isError = false;
      this.quotes = Object.keys(response.data).map((currency) => ({
        ...response.data[currency],
        currency,
      }));
    }
  }

  async fetchQuotes() {
    const response = await fetchQuotes("/public?command=returnTicker");
    if (response.data.error) {
      this.quotes = [];
      this.isError = true;
    } else {
      this.isError = false;
      this.quotes = Object.keys(response.data).map((currency) => ({
        ...response.data[currency],
        currency,
      }));
    }
  }
}

const quotes = new Quotes();

export default quotes;
