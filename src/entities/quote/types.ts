export interface IQuote {
  baseVolume: string;
  currency: string;
  high24hr: string;
  highestBid: string;
  id: number;
  isFrozen: string;
  last: string;
  low24hr: string;
  lowestAsk: string;
  percentChange: string;
  postOnly: string;
  quoteVolume: string;
}

export interface IQuoteTableColumn {
  currency: string;
  last: string;
  highestBid: string;
  percentChange: string;
}
