import styles from "./styles.module.scss";

const ErrorTooltip = () => (
  <div className={styles.wrapper}>Something went wrong!</div>
);

export default ErrorTooltip;
