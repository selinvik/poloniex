import styles from "./styles.module.scss";

interface IButtonProps {
  children: React.ReactNode;
}

const Button = ({ children }: IButtonProps) => (
  <button className={styles.button}>{children}</button>
);

export default Button;
