import { IQuote, IQuoteTableColumn } from "entities/quote/types";
import { Column, useTable } from "react-table";

import { Loader } from "shared/ui";

import styles from "./styles.module.scss";

interface IQuotesTableProps {
  isLoading: boolean;
  data: IQuote[];
  columns: Column<IQuoteTableColumn>[];
  onRowClick: (quote: IQuote) => void;
}

const QuotesTable = ({
  isLoading,
  data,
  columns,
  onRowClick,
}: IQuotesTableProps) => {
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable({ data, columns, defaultColumn: { width: "100%" } });

  if (isLoading) {
    return (
      <div className={styles.loaderWrapper}>
        <Loader />
      </div>
    );
  }

  return (
    <div className={styles.tableWrapper}>
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th
                  {...column.getHeaderProps({
                    style: {
                      width: column.width,
                    },
                  })}
                >
                  {column.render("Header")}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {rows.map((row) => {
            prepareRow(row);
            return (
              <tr
                {...row.getRowProps()}
                onClick={() => onRowClick(row.original as IQuote)}
              >
                {row.cells.map((cell) => (
                  <td
                    {...cell.getCellProps({
                      style: {
                        width: cell.column.width,
                      },
                    })}
                  >
                    {cell.render("Cell")}
                  </td>
                ))}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default QuotesTable;
