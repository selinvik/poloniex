import { ModalContext } from "app/context";
import { SyntheticEvent, useContext } from "react";
import ReactDOM from "react-dom";

import styles from "./styles.module.scss";

interface IModalProps {
  children: React.ReactNode;
}

const Modal = ({ children }: IModalProps) => {
  const { closeModal } = useContext(ModalContext);

  const handleOvarlayClick = (e: SyntheticEvent) => {
    if (e.target === e.currentTarget) {
      closeModal();
    }
  };

  return ReactDOM.createPortal(
    <div className={styles.overlay} onClick={handleOvarlayClick}>
      <div className={styles.content}>{children}</div>
    </div>,
    document.getElementById("modal-root") as HTMLElement
  );
};

export default Modal;
