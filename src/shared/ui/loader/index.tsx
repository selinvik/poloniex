import styles from "./styles.module.scss";

const Loader = () => <div className={styles.loader} />;

export default Loader;
