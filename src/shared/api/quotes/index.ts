import axios from "axios";

export const fetchQuotes = async (url: string) => {
  const response = await axios.get(url);

  return response;
};
