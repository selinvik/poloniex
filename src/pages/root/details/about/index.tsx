import styles from "./styles.module.scss";

const About = () => (
  <main>
    <header>
      <h1 className={styles.h1}>Welcome to our app!</h1>
    </header>
  </main>
);

export default About;
