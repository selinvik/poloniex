import { IQuote } from "entities/quote/types";
import styles from "./styles.module.scss";

interface IQuoteInfoProps {
  quote: IQuote;
}

const QuoteInfo = ({ quote }: IQuoteInfoProps) => (
  <div className={styles.wrapper}>
    <p className={styles.title}>{quote.currency}</p>
    <div className={styles.listWrapper}>
      <div className={styles.list}>
        {Object.keys(quote)
          .filter((allKeys) => allKeys !== "currency")
          .map((filteredKey: IQuote[keyof IQuote]) => (
            <span key={filteredKey} className={styles.listItem}>
              <span className={styles.listItemTitle}>{filteredKey}</span>
              <span className={styles.listItemValue}>
                {quote[filteredKey as keyof IQuote]}
              </span>
            </span>
          ))}
      </div>
    </div>
  </div>
);

export default QuoteInfo;
