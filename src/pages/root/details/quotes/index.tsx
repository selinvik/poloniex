import { ModalContext } from "app/context";
import { quotesStore } from "app/store";
import { observer } from "mobx-react-lite";
import React, { useContext, useEffect } from "react";
import { ErrorTooltip, QuotesTable } from "shared/ui";
import { QuoteInfo } from "./modals";

import "./styles.module.scss";
import { Column } from "react-table";
import { IQuote, IQuoteTableColumn } from "entities/quote/types";
import { configColumns } from "./config";

const Quotes = observer(() => {
  const { openModal, modal } = useContext(ModalContext);

  const onRowClick = (quote: IQuote) => {
    openModal(<QuoteInfo quote={quote} />);
  };

  useEffect(() => {
    let interval: ReturnType<typeof setInterval>;

    if (!modal) {
      interval = setInterval(() => {
        quotesStore.fetchQuotes();
      }, 5000);
    }

    return () => {
      clearInterval(interval);
    };
  }, [modal]);

  useEffect(() => {
    quotesStore.initialFetchQuotes();
  }, []);

  const columns: Column<IQuoteTableColumn>[] = React.useMemo(
    () => configColumns as Column<IQuoteTableColumn>[],
    []
  );

  return (
    <main>
      <header>
        <h1>Quotes</h1>
        {quotesStore.isError && <ErrorTooltip />}
      </header>
      <section>
        <QuotesTable
          isLoading={quotesStore.isLoading}
          data={quotesStore.quotes}
          columns={columns}
          onRowClick={onRowClick}
        />
      </section>
    </main>
  );
});

export default Quotes;
