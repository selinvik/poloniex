import { NavLink, Outlet } from "react-router-dom";
import styles from "./styles.module.scss";

import { Quotes, About } from "./details";
import { useMediaQuery } from "react-responsive";

const links = [
  {
    id: 0,
    path: "",
    title: "About",
  },
  {
    id: 1,
    path: "quotes",
    title: "Quotes",
  },
];

const Root = () => {
  const isMobile = useMediaQuery({ query: `(max-width: 599px)` });

  if (isMobile) {
    return (
      <div className={styles.mobileRoot}>
        <nav>
          <ul>
            {links.map((item) => (
              <li key={item.id}>
                <NavLink
                  to={item.path}
                  className={({ isActive }) => (isActive ? styles.active : "")}
                >
                  {item.title}
                </NavLink>
              </li>
            ))}
          </ul>
        </nav>
        <Outlet />
      </div>
    );
  }

  return (
    <div className={styles.root}>
      <nav>
        <ul>
          {links.map((item) => (
            <li key={item.id}>
              <NavLink
                to={item.path}
                className={({ isActive }) => (isActive ? styles.active : "")}
              >
                {item.title}
              </NavLink>
            </li>
          ))}
        </ul>
      </nav>
      <Outlet />
    </div>
  );
};

Root.About = About;
Root.Quotes = Quotes;

export default Root;
